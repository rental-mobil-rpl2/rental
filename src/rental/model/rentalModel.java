/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.model;

import rental.event.rentalListener;
import javax.swing.JOptionPane;

/**
 *
 * @author gu
 */
public class rentalModel {
    private String nik;
    private String no_plat;
    private String tgl_pinjam;
    private String durasi;
    private String total_bayar;
    private String nama_pelanggan;
    private String email;
    private String alamat;
    private String no_telp;
    

    private rentalListener rentalListener;

    public rentalListener getRentalListener() {
        return rentalListener;
    }

    public void setRentalListener(rentalListener rentalListener) {
        this.rentalListener = rentalListener;
    }

    public String getNama_pelanggan() {
        return nama_pelanggan;
    }

    public void setNama_pelanggan(String nama_pelanggan) {
        this.nama_pelanggan = nama_pelanggan;
        fireOnChange();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        fireOnChange();
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
        fireOnChange();
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
        fireOnChange();
    }
    
    
    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
        fireOnChange();
    }

    public String getNo_plat() {
        return no_plat;
    }

    public void setNo_plat(String no_plat) {
        this.no_plat = no_plat;
        fireOnChange();
    }

    public String getTgl_pinjam() {
        return tgl_pinjam;
    }

    public void setTgl_pinjam(String tgl_pinjam) {
        this.tgl_pinjam = tgl_pinjam;
        fireOnChange();
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
        fireOnChange();
    }

    public String getTotal_bayar() {
        return total_bayar;
    }

    public void setTotal_bayar(String total_bayar) {
        this.total_bayar = total_bayar;
        fireOnChange();
    }
    
    protected void fireOnChange(){
        if(rentalListener != null){
            rentalListener.onChange(this);
        }
    }
    
    
    public void simpanForm(){
        JOptionPane.showMessageDialog(null, "Booking Berhasil, Silahkan Tunggu Pesan Dari Kami Untuk Konfirmasi");
    }
}
