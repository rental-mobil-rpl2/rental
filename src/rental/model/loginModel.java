/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.model;

import rental.event.loginListener;
import rental.view.FormLogin;
import rental.view.FormRental;

/**
 *
 * @author Gannn
 */
public class loginModel {
    private String username;
    private String password;
    
    private loginListener loginListener;

    public loginListener getLoginListener() {
        return loginListener;
    }

    public void setLoginListener(loginListener loginListener) {
        this.loginListener = loginListener;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        fireOnChange();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        fireOnChange();
    }
    
    protected void fireOnChange(){
        if(loginListener != null){
            loginListener.onChange(this);
        }
    }
    
    public void resetForm(){
        setUsername("");
        setPassword("");   
    }
    
    public void pindah(){
        new FormLogin().setVisible(false);
        new FormRental().setVisible(true);
    }
    
    public void login(){
        resetForm();
    }
}
