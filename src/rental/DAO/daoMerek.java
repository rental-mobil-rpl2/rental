/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.DAO;
import rental.koneksi.koneksi;
import rental.model.merek;
import rental.DAOImplement.implementMerek;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Fajar
 */
public class daoMerek implements implementMerek{
    
    Connection connection;
    final String insert = "INSERT INTO merek (merek) VALUES (?);";
    final String update = "UPDATE merek set merek=? where id_merek=? ;";
    final String delete = "DELETE FROM merek where id_merek=? ;";
    final String select = "SELECT * FROM merek;";
    
    public daoMerek() {
        connection = koneksi.connection();
    }
    
    public void insert(merek b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getMerek());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                b.setId_merek(rs.getInt(1));
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(merek b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getMerek());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<merek> getALL() {
        List<merek> lb = null;
        try {
            lb = new ArrayList<merek>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                merek b = new merek();
                b.setId_merek(rs.getInt("id_merek"));
                b.setMerek(rs.getString("merek"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoMerek.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }

//    public List<mahasiswa> getCariNama(String nama) {
//        List<mahasiswa> lb = null;
//        try {
//            lb = new ArrayList<mahasiswa>();
//            PreparedStatement st = connection.prepareStatement(carinama);
//            st.setString(1, "%" + nama + "%");
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                mahasiswa b = new mahasiswa();
//                b.setId(rs.getInt("id"));
//                b.setNim(rs.getString("nim"));
//                b.setNama(rs.getString("nama"));
//                b.setJk(rs.getString("jk"));
//                b.setAlamat(rs.getString("alamat"));
//                lb.add(b);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(daoMahasiswa.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lb;
//    }

}
