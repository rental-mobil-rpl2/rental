/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.controller;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import static java.lang.Integer.parseInt;
import javax.swing.JOptionPane;
import java.sql.*;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;
import rental.model.databaseModel;
import rental.view.FormRental;
import rental.model.rentalModel;
/**
 *
 * @author Gannn
 */
public class rentalControlller {
    private databaseModel db;
    private rentalModel rentalModel;

    public rentalModel getRentalModel() {
        return rentalModel;
    }

    public void setRentalModel(rentalModel rentalModel) {
        this.rentalModel = rentalModel;
    }
    
    public void simpanForm(FormRental view) {
        String nik = view.getTxtNik().getText();
        String no_plat = view.getjComboPlat().getSelectedItem().toString();
        String tgl_pinjam = view.getjComboTglMulai().getSelectedItem().toString();
        Integer durasi = parseInt(view.getjComboDurasi().getSelectedItem().toString());
        Integer total_bayar = parseInt(view.getTxtTotal().getText());

        if (nik.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "NIK tidak boleh kosong!");
        } else if (total_bayar.equals("")) {
            JOptionPane.showMessageDialog(view, "Total Harga tidak boleh kosong!");
        } else {
            if(Insert(nik, no_plat, tgl_pinjam, durasi,total_bayar)){
                rentalModel.simpanForm();
            }
        }
    }
    
    public boolean Insert(String nik, String no_plat,String tgl_pinjam, Integer durasi, Integer total_bayar) {
        boolean result = false;
        Statement stmt = db.createStmt();
        String query = "INSERT INTO sewa(nik,no_plat,tgl_pinjam,durasi,total_bayar) VALUES('" + nik + "','" + no_plat + "','" + tgl_pinjam + "','" + durasi + "','" + total_bayar + "')";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public void simpanPelanggan(FormRental view) {
        String nik = view.getTxtNik().getText();
        String nama_pelanggan = view.getTxtNama().getText();
        String email = view.getTxtEmail().getText();
        String alamat = view.getTxtAlamat().getText();
        String no_telp = view.getTxtNotelp().getText();

        if (nama_pelanggan.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Nama Pelanggan tidak boleh kosong!");
        } else if (email.equals("")) {
            JOptionPane.showMessageDialog(view, "Email tidak boleh kosong!");
        } else if (alamat.equals("")) {
            JOptionPane.showMessageDialog(view, "Alamat tidak boleh kosong!");
        } else if (no_telp.equals("")) {
            JOptionPane.showMessageDialog(view, "No. Telp tidak boleh kosong!");
        } else {
            if(InsertPelanggan(nik, nama_pelanggan, email, alamat,no_telp)){
            }
        }
    }
    
    public boolean InsertPelanggan(String nik, String nama_pelanggan,String email, String alamat, String no_telp) {
        boolean result = false;
        Statement stmt = db.createStmt();
        String query = "INSERT INTO pelanggan(nik,nama_pelanggan,email,alamat,no_telp) VALUES('" + nik + "','" + nama_pelanggan + "','" + email + "','" + alamat + "','" + no_telp + "')";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public void ubahStatus(FormRental view) {
        String no_plat = view.getjComboPlat().getSelectedItem().toString();

        if (no_plat.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "No Plat tidak boleh kosong!");
        } else {
            if(UpdateStatus(no_plat)){
            }
        }
    }
    
    public boolean UpdateStatus(String no_plat) {
        boolean result = false;
        Statement stmt = db.createStmt();
        String query = "UPDATE mobil SET status='booked' WHERE no_plat = '"+ no_plat +"' ";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public void refreshTable(JTable table){
        Statement stmt = db.createStmt();
        try{
            ResultSet rs = stmt.executeQuery("SELECT * FROM mobil WHERE status='booked' ");
            table.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
