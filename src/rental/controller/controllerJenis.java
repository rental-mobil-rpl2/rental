/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.controller;
import rental.DAO.daoJenis;
import rental.DAOImplement.implementJenis;
import rental.model.jenis;
import rental.model.tableModelJenis;
import rental.view.FormJenis;
import java.util.List;
import javax.swing.JOptionPane;
/**
 *
 * @author Akmal
 */
public class controllerJenis {
     FormJenis frame;
    implementJenis implJenis;
    List<jenis> lb;

    public controllerJenis(FormJenis frame) {
        this.frame = frame;
        implJenis = new daoJenis();
        lb = implJenis.getALL();
    }

    //mengosongkan field
    public void reset() {
//        frame.getTxtId_jenis().setText("");
        frame.getTxtJenis().setText("");
    }

    //menampilkan data ke dalam tabel
    public void isiTable() {
        lb = implJenis.getALL();
        tableModelJenis tmb = new tableModelJenis(lb);
        frame.getTabelData().setModel(tmb);
    }

    //merupakan fungsi untuk menampilkan data yang dipilih dari tabel
    public void isiField(int row) {
        frame.getTxtID().setText(lb.get(row).getId_jenis().toString());
        frame.getTxtJenis().setText(lb.get(row).getJenis());
    }

    //merupakan fungsi untuk insert data berdasarkan inputan user dari textfield di frame
    public void insert() {
        
      if (!frame.getTxtJenis().getText().trim().isEmpty()& !frame.getTxtJenis().getText().trim().isEmpty()) {
          
        jenis b = new jenis();
        b.setJenis(frame.getTxtJenis().getText());
        
        implJenis.insert(b);
        JOptionPane.showMessageDialog(null, "Simpan Data sukses");
        
        } else {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }

    //berfungsi untuk update data berdasarkan inputan user dari textfield di frame
    public void update() {
   if (!frame.getTxtID().getText().trim().isEmpty()) {
             
        jenis b = new jenis();
        b.setJenis(frame.getTxtJenis().getText());
        implJenis.update(b);
        
        JOptionPane.showMessageDialog(null, "Update Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di ubah");
        }
    }

    //berfungsi menghapus data yang dipilih
    public void delete() {
        if (!frame.getTxtID().getText().trim().isEmpty()) {
            int id = Integer.parseInt(frame.getTxtID().getText());
            implJenis.delete(id);
            
            JOptionPane.showMessageDialog(null, "Hapus Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di hapus");
        }
    }

//    public void isiTableCariNama() {
//        lb = implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//        tableModelMahasiswa tmb = new tableModelMahasiswa(lb);
//        frame.getTabelData().setModel(tmb);
//    }
//
//    public void carinama() {
//        if (!frame.getTxtCariNama().getText().trim().isEmpty()) {
//            implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//            isiTableCariNama();
//        } else {
//            JOptionPane.showMessageDialog(frame, "SILAHKAN PILIH DATA");
//        }
//    }
}
