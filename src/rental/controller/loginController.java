/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental.controller;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import static java.lang.Integer.parseInt;
import javax.swing.JOptionPane;
import rental.model.loginModel;
import rental.model.databaseModel;
import rental.view.FormLogin;
import static java.lang.Integer.parseInt;
import java.sql.*;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;
/**
 *
 * @author Gannn
 */
public class loginController {
    private databaseModel db;
    private loginModel loginModel;

    public loginModel getLoginModel() {
        return loginModel;
    }

    public void setLoginModel(loginModel loginModel) {
        this.loginModel = loginModel;
    }
    
    public void resetForm(FormLogin view) {
        String username = view.getTxtUsername().getText();
        String password = view.getTxtPassword().getText();
        
        if (username.equals("") && password.equals("")) {
        } else {
            loginModel.resetForm();
        }
    }
    
    public void login(FormLogin view) {
        String username = view.getTxtUsername().getText();
        String password = view.getTxtPassword().getText();

        if (username.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Username tidak boleh kosong!");
        } else if (password.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Password tidak boleh kosong!");
        } else {
            if(Masuk(username, password)){
                loginModel.login();
            }
        }
    }
    
    public boolean Masuk(String username, String password) {
        boolean result = false;
        Statement stmt = db.createStmt();
        ResultSet rs;
        try {
            String query = "SELECT * FROM admin WHERE username='"+ username +"' AND password='"+ password +"' ";
            rs = stmt.executeQuery(query);
            if(rs.next()){
                if(username.equals(rs.getString("username")) && password.equals(rs.getString("password"))){
                    JOptionPane.showMessageDialog(null, "berhasil login");
                    loginModel.pindah();
                }
            }else{
                    JOptionPane.showMessageDialog(null, "username atau password salah");
                }
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
